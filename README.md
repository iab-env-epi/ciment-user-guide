# IAB Equipe 12 CIMENT User Guide

The HTML version of this guide is at [iab-env-epi.gricad-pages.univ-grenoble-alpes.fr/ciment-user-guide](https://iab-env-epi.gricad-pages.univ-grenoble-alpes.fr/ciment-user-guide)

To update the user guide:

1. Edit `ciment-user-guide.Rmd`

2. If using RStudio, click the "Knit" button. Otherwise, in R run `rmarkdown::render("ciment-user-guide.Rmd", output_file = "index.html", output_dir = "public")`.

3. Commit the changes (be sure to include any changes to the `public` dir) and push
